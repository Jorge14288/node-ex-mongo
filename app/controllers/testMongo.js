const Product = require('../models/product.model');
const mongoose = require('mongoose');
// Connect to the beerlocker MongoDB
mongoose.connect('mongodb://localhost:27017/Test');
controller = {};

controller.getAll = function(req, res, next) {
    console.log("Test Mongo db");
    var model = new Product();
    console.log(model);

    Product.find(function(err, products) {
        console.log('find all');
        if (err) {
            res.send(err);
        }
        res.json(products);
    })
}
controller.getId = function(req, res, next) {
    console.log('find one');
    let id = req.params.id;
    
    if(id==null ||  id==''){
        var resp ={
            code : 400,
            message : "ERROR: Debe de enviar el id del producto a buscar"
        }
        res.json(resp);
    }
    Product.findById(id, function(err, beer) {
        if (err)
            res.send(err);
        res.json(beer);
    });

}
controller.create = function(req, res, next) {
    console.log('Post Product');
    var test = new Product();

    test.name = 'fisrt';
    test.price = 22;

    console.log(test);
    test.save(function(err) {
        console.log('save function');
        if (err)
            res.send(err);
        res.json({ message: 'Product added to the locked! ', data: test });
    });
}
controller.update = function(req, res, next) {
    console.log('update test')
    //id = '5cf3402bfd06c02320a06fb4';
    let id = req.body.id;
    
    if(id==null ||  id==''){
        var resp ={
            code : 400,
            message : "ERROR: Debe de enviar el id del producto a buscar"
        }
        res.json(resp);
    }

    Product.findById(id, function(err, product) {
        if (err)
            res.send(err);

       // product.name = "modificado";
       // product.price = 50;

        product.name = req.body.name;
        product.price = req.body.price;

        product.save(function(err) {
            if (err)
                res.send(err);
            //res.json(product);
            res.json({ message: 'Product update to the locked! ', data: product });
        })
    });
}
controller.delete = function(req, res, next) {
    console.log('delete');
    id = '5ba95511199a3526b00224eb';
    Product.findByIdAndRemove(id, function(err) {
        if (err)
            res.send(err);
        res.json({ message: 'Product removed from the locker!' });
    });
}
module.exports = controller;