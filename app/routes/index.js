var express = require('express');
var router = express.Router();
const Product = require('../models/product.model');

var controllers = require('../controllers/index');


/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});



//TEST MONGO
router.get('/product', controllers.mongo.getAll);
router.get('/product/:id', controllers.mongo.getId);
router.post('/product', controllers.mongo.create);
router.put('/product', controllers.mongo.update);
router.delete('/product', controllers.mongo.delete);

//lOGIN
router.post('/login', controllers.login.postLogin);

module.exports = router;