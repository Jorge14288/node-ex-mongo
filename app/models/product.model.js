const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ProductSchema = new Schema({
    name: { type: String, requiere: true, max: 100 },
    price: { type: Number, requiere: true },
})

//Export the model
module.exports = mongoose.model('Product', ProductSchema);