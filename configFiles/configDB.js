const configSqlServer = {
    user: "test",
    password: "amor",
    server: "ASUS\\SQLEXPRESS",
    database: "PruebasDb",
    pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 30000
    }
}

const configDBMysql = {
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'busticketdb'
}

const configPg = {
    user: 'rob',
    database: 'BusAppDB',
    password: 'rob',
    port: 5432,
    max: 10, // max number of connection can be open to database
    idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
};
module.exports.configSqlServer = configSqlServer;
module.exports.configDBMysql = configDBMysql;
module.exports.configPg = configPg;